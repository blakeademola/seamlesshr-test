<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */


use App\Course;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {
    return [
        'text' => $faker->sentence (20),
        'title' => $faker->sentence (3),
        'author' => $faker->name,
        'course_duration' => $faker->numberBetween(5, 200)
    ];
});
