<?php

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Seeder;

class CourseSeeder extends Seeder implements ShouldQueue
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Course::class, 50)->create();

    }
}
