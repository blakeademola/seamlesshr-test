<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Routes that do not require authentication

Route::post('/register', 'UserController@register');
Route::get('/login', 'UserController@authenticate');

//Routes that require authentication and wrapped in the userAccess middleware
Route::group(['middleware' => ['userAccess']], function () {
    Route::get('/allcourses', 'CourseController@allCourses');
    Route::get('/createcourse', 'CourseController@createCourse');
    Route::get('/registercourse', 'CourseController@registercourse');
    Route::get('/user', 'UserController@getAuthenticatedUser');
    Route::get('/export', 'CourseController@exportcourses');
});