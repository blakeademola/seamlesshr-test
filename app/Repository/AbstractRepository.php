<?php
/**
 * Created by PhpStorm.
 * User: bolade
 * Date: 7/10/19
 * Time: 12:13 PM
 */

namespace App\Repository;


use Illuminate\Database\Eloquent\Model;

class AbstractRepository
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function find($id)
    {
        return $this->model->find($id, $cols = array('*'));
    }

    public function findWhere($id, $val)
    {
        $result = $this->model->all()->where($val, '=', $id);
        return $result;
    }

    public function create($data)
    {
        $model = $this->getNew($data);

        return $model->save();
    }

    public function getNew(array $attributes = array())
    {
        return $this->model->newInstance($attributes);
    }
}