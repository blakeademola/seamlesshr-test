<?php
namespace App\Eloquent\Interfaces;

interface UserInterface
{
    public function getAll();

    public function find($id);

    public function create($data);
}