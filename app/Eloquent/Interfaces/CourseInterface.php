<?php
namespace App\Eloquent\Interfaces;

interface CourseInterface
{
    public function getAll();

    public function find($id);

    public function findWhere($id, $val);

    public function create($data);

}