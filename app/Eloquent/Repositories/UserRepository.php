<?php

namespace App\Eloquent\Repositories;

use App\Eloquent\Interfaces\UserInterface;
use App\Repository\AbstractRepository;
use App\User;

class UserRepository extends AbstractRepository implements UserInterface
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
        parent::__construct($user);
    }


}