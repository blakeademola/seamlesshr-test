<?php

namespace App\Eloquent\Repositories;

Use App\Course;
use App\Eloquent\Interfaces\CourseInterface;
use App\Repository\AbstractRepository;


class CourseRepository extends AbstractRepository implements CourseInterface
{

    protected $course;

    public function __construct(Course $course)
    {
        $this->course = $course;
        parent::__construct($course);
    }


}