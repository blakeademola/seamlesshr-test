<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $guarded = [];

    public function users()
    {
        return $this->belongsToMany(User::class, 'users', 'course_id', 'user_id');
    }
    public function userCourse(){
        return $this->belongsTo(UserCourse::class,'id','course_id');
    }
}
