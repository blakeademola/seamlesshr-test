<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class courseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'text' => $this->text,
            'author' => $this->author,
            'course duration' => $this->course_duration,
            'enroll date'=>[
                'date of enrollment'=>$this->userCourse !== null ? Carbon::parse($this->userCourse->created_at)->format('d M, Y') : 'Not Enrolled'
            ]
        ];
    }
}
