<?php

namespace App\Http\Controllers;

use App\Course;
use App\Eloquent\Interfaces\CourseInterface;
use App\Eloquent\Interfaces\UserInterface;
use App\Exports\CoursesExport;
use App\Http\Resources\courseResource;
use App\Jobs\CreateCourses;
use App\UserCourse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Jobs\QueuedCourseJobs;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Tymon\JWTAuth\Facades\JWTAuth;

class CourseController extends Controller
{
    protected $course, $user;

    public function __construct(CourseInterface $course, UserInterface $user)
    {
        $this->course = $course;
        $this->user = $user;
    }

    public function allCourses()
    {
//        return a transformed result of all courses
        return courseResource::collection(Course::all());

    }

    public function createCourse()
    {
        try {
            CreateCourses::dispatch()->delay(Carbon::now()->addMinutes(1));
            return response()->json([
                'status' => 200,
                'message' => "Courses' creation has been queued!"
            ]);
        } catch (\Exception $e) {
            report($e);

            return response()->json([
                'status' => 500,
                'message' => 'Something went wrong!'
            ]);
        }
    }

//Register a course and in turn map the course registered for to a user
    public function registercourse(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'course_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->getMessageBag(),
                'code' => 400
            ]);
        }
        $course = $this->course->findWhere($request->course_id, 'id')->first();
        UserCourse::firstOrcreate([
            'user_id' => JWTAuth::parseToken()->authenticate()->id,
            'course_id' => $course->id
        ]);
        return response()->json([
            'Success' => "Successfully registered for $course->title",
            'code' => 200
        ], 200);

    }

    public function exportcourses(Request $request)
    {
        switch (strtolower($request->type)) {
            case "csv":
//        Instantiate the CoursesExport class that holds the Course Model and download as csv format
                return Excel::download(new CoursesExport, 'courses.csv');

            case "xls":
//        Instantiate the CoursesExport class that holds the Course Model and download as csv format
                return Excel::download(new CoursesExport, 'courses.xls');

            default:
                return "set a format using 'format' as the key. Only formats available are csv and xls";
        }
    }
}
