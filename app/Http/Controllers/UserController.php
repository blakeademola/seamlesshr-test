<?php

namespace App\Http\Controllers;

use App\Eloquent\Interfaces\CourseInterface;
use App\Eloquent\Interfaces\UserInterface;
use App\Jobs\QueuedCourseJobs;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;


class UserController extends Controller
{
    protected $course, $user;

    public function __construct(CourseInterface $course, UserInterface $user)
    {
        $this->course = $course;
        $this->user = $user;
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid login details'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could not create token'], 500);
        }

        return response()->json([
            'Message' => "Welcome $request->email",
            'code' => 200,
            'Token' => compact('token'),
        ]);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:5',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->getMessageBag(),
                'code' => 400
            ]);
        }
        $data = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
        ];

        $user = User::create($data);

        $token = JWTAuth::fromUser($user);

        return response()->json(compact('user', 'token'), 201);

    }

    public function getAuthenticatedUser()
    {
        try {

            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getCode());

        } catch (TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getCode());

        } catch (JWTException $e) {

            return response()->json(['token_absent'], $e->getCode());

        }

        return response()->json(compact('user'));
    }
}
