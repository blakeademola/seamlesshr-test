<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Eloquent\Interfaces\UserInterface', 'App\Eloquent\Repositories\UserRepository');
        $this->app->bind('App\Eloquent\Interfaces\CourseInterface', 'App\Eloquent\Repositories\CourseRepository');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
Schema::defaultStringLength(191);
    }
}
